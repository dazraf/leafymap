# Leafy Maps

This is an experiment to demonstrate the use of 

* [Leaflet JS](https://leafletjs.com/)
* [MapBox](https://www.mapbox.com/)
* [KotlinJS](https://kotlinlang.org/docs/reference/js-overview.html)

To serve a HTML view with a map **using locally served map tiles** 
preloaded from MapBox during the build.

The build of the project automatically downloads the MapBox tiles using the [`tiler`](tiler) module.

### Build

Ensure that you have a MapBox account and that your MapBox token is 
stored in the environment variable `MAPBOX_TOKEN`.

Then, run a maven build:

`mvn clean install`

### Run 

Three ways of doing this:

1. Docker
2. Maven
3. Old school static file webserver

#### 1. Docker

```
docker build -t dazraf/leafymaps-server:latest .
docker run -p 8080:8080 dazraf/leafymaps-server:latest`
```

#### 2. Maven

`mvn -pl server exec:java`

#### 3. Old School

Any static file server on `web/target/classes` will work. e.g.
> `cd web/target/classes && python -m SimpleHTTPServer 8000`


### Deployment

Build is deployed to gitlab pages - https://dazraf.gitlab.io/leafymap/