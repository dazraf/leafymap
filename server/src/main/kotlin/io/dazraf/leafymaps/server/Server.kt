package io.dazraf.leafymaps.server

import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler

fun main(args: Array<String>) {
  val vertx = Vertx.vertx()
  val router = Router.router(vertx)
  router.get().handler(StaticHandler.create("web"))
  val port = 8080
  vertx.createHttpServer()
    .requestHandler(router)
    .listen(port) {
      when {
        it.failed() -> {
          println("failed to startup: ${it.cause().message}")
          it.cause().printStackTrace()
          vertx.close()
        }
        else -> {
          println("started on: http://localhost:$port")
        }
      }
    }
}