package io.dazraf.leafymaps.tiler

import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import java.io.File

const val host = "api.tiles.mapbox.com"
const val rootDir = "tiler/target/tiles"
const val concurrentRequests = 4
const val style = "mapbox.light"

val defaultWebClientOptions = WebClientOptions()
  .setDefaultHost(host)
  .setDefaultPort(443)
  .setSsl(true)
  .setTrustAll(true)!!
val accessToken = System.getenv("MAPBOX_TOKEN")!!
val vertx = Vertx.vertx()!!

fun request(params: RequestParams): Future<Buffer> {
  val result = Future.future<Buffer>()
  val wc = WebClient.create(vertx, defaultWebClientOptions)
  println("getting ${params.url}")
  wc.get(params.url).send {
    when {
      it.failed() -> result.fail(it.cause())
      else -> {
        val response = it.result()
        when (response.statusCode() / 100) {
          2 -> result.complete(response.bodyAsBuffer())
          else -> result.fail("failed to get https://$host${params.url}: ${response.statusCode()} - ${response.statusMessage()}")
        }
      }
    }
  }
  return result
}

fun write(path: String, buffer: Buffer): Future<Unit> {
  val file = File(path).apply { parentFile.mkdirs() }
  val result = Future.future<Void>()
  vertx.fileSystem().writeFile(file.absolutePath, buffer) {
    result.completer().handle(it)
  }
  return result.onSuccess { println("wrote $path") }.mapUnit()
}

fun get(x: Int, y: Int, z: Int): Future<Unit> {
  val params = RequestParams(x, y, z, accessToken, style)
  return request(params)
    .compose { write(params.filePath(rootDir), it) }
}

fun getAll(zoomLevel: Int): Future<Unit> {
  val tiles = (1 shl zoomLevel) * (1 shl zoomLevel)
  val side = Math.sqrt(tiles.toDouble()).toInt()
  val coords = (0 until side).flatMap { x -> (0 until side).map { y -> x to y } }
  return coords
    .groupBy { (x, y) -> (x * side + y) % concurrentRequests }
    .values.fold(succeededFuture()) {f, group ->
      f.compose {
        group.map { (x, y) -> get(x, y, zoomLevel) }.all().mapUnit()
      }
    }
}

class App {
  fun run() {
    LogInitialiser.init()
    val zoomLevels = (0 .. 4)
    zoomLevels.fold(succeededFuture<Unit>()) { f, z -> f.compose { getAll(z) }}
      .onSuccess {
        println("finished")
        vertx.close()
      }
      .catch {
        println("failed with: ${it.message}")
        it.printStackTrace()
        vertx.close()
      }
  }
}

fun main(args: Array<String>) {
  App().run()
}




