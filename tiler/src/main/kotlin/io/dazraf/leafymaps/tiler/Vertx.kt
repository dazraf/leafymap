package io.dazraf.leafymaps.tiler

import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Future.future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.file.FileSystem
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger

inline fun <reified T : Any> loggerFor(): Logger = LoggerFactory.getLogger(T::class.java)
private object VertxX
private val logger = loggerFor<VertxX>()


object LogInitialiser {
    init {
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory")
    }

    fun init() {
        // will cause init to be called once and once only
    }
}

fun <T> Vertx.executeBlocking(fn: () -> T): Future<T> {
    val result = Future.future<T>()
    this.executeBlocking({ f: Future<T> ->
        try {
            f.complete(fn())
        } catch (err: Throwable) {
            f.fail(err)
        }
    }) {
        result.completer().handle(it)
    }
    return result
}

fun <T> Future<T>.onSuccess(fn: (T) -> Unit): Future<T> {
    val result = Future.future<T>()
    setHandler {
        try {
            if (it.succeeded()) {
                fn(it.result())
            }
            result.completer().handle(it)
        } catch (err: Throwable) {
            result.fail(err)
        }
    }
    return result
}

fun <T> Future<T>.catch(fn: (Throwable) -> Unit): Future<T> {
    val result = Future.future<T>()
    setHandler {
        try {
            if (it.failed()) {
                fn(it.cause())
            }
            result.completer().handle(it)
        } catch (err: Throwable) {
            result.fail(err)
        }
    }
    return result
}

fun <T> Future<T>.finally(fn: (AsyncResult<T>) -> Unit): Future<T> {
    val result = Future.future<T>()
    setHandler {
        try {
            fn(it)
            result.completer().handle(it)
        } catch (err: Throwable) {
            result.fail(err)
        }
    }
    return result
}

fun <T> List<Future<T>>.all(): Future<List<T>> {
    if (this.isEmpty()) return succeededFuture(emptyList())
    val results = mutableMapOf<Int, T>()
    val fResult = future<List<T>>()
    val countdown = AtomicInteger(this.size)
    this.forEachIndexed { index, future ->
        future.setHandler { ar ->
            when {
                ar.succeeded() && fResult.succeeded() -> {
                    logger.error("received a successful result in List<Future<T>>.all after all futures where apparently completed!")
                }
                fResult.failed() -> {
                    // we received a result after the future was deemed failed. carry on.
                }
                ar.succeeded() -> {
                    results[index] = ar.result()
                    if (countdown.decrementAndGet() == 0) {
                        fResult.complete(results.entries.sortedBy { it.key }.map { it.value })
                    }
                }
                else -> {
                    // we've got a failed future - report it
                    fResult.fail(ar.cause())
                }
            }
        }
    }
    return fResult
}

fun <T> Future<T>.mapUnit() : Future<Unit> {
    return this.map { Unit }
}

@JvmName("allTyped")
fun <T> all(vararg futures: Future<T>): Future<List<T>> {
    return futures.toList().all()
}

@Suppress("UNCHECKED_CAST")
fun all(vararg futures: Future<*>): Future<List<*>> {
    return (futures.toList() as List<Future<Any>>).all() as Future<List<*>>
}

fun FileSystem.mkdirs(path: String): Future<Void> {
    return withFuture { mkdirs(path, it.completer()) }
}

fun FileSystem.readFile(path: String): Future<Buffer> {
    return withFuture { readFile(path, it.completer()) }
}

fun FileSystem.writeFile(path: String, byteArray: ByteArray): Future<Void> {
    return withFuture { writeFile(path, Buffer.buffer(byteArray), it.completer()) }
}

fun FileSystem.readDir(path: String): Future<List<String>> {
    return withFuture { readDir(path, it.completer()) }
}

fun FileSystem.copy(from: String, to: String): Future<Void> {
    return withFuture { copy(from, to, it.completer()) }
}

fun FileSystem.readFiles(dirPath: String): Future<List<Pair<String, Buffer>>> {
    return readDir(dirPath)
        .compose { files ->
            files.map { file ->
                readFile(file).map { buffer -> file to buffer }
            }.all()
        }
}

fun FileSystem.deleteFile(filePath: String): Future<Unit> {
    return withFuture { future ->
        delete(filePath) {
            if (it.succeeded()) {
                future.complete(Unit)
            } else {
                future.fail(it.cause())
            }
        }
    }
}

inline fun <T> withFuture(fn: (Future<T>) -> Unit): Future<T> {
    val result = future<T>()
    fn(result)
    return result
}

fun <T> Future<T>.completeFrom(value: T?, err: Throwable?) {
    return when {
        err != null -> fail(err)
        else -> complete(value)
    }
}

