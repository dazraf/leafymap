package io.dazraf.leafymaps.tiler

import java.io.File

data class RequestParams(
  val x: Int,
  val y: Int,
  val z: Int,
  val accessToken: String,
  val style: String = "mapbox.light"
) {
  private val path = "/$z/$x/$y.png"
  val url = "/v4/$style$path?access_token=$accessToken"
  fun filePath(parent: String): String {
    return File(parent, path).absolutePath
  }
}