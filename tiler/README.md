# Tiler

This module downloads the tiles from [MapBox](https://www.mapbox.com) to the web resources. 
You will need your mapbox token available in the system environment variable `MAPBOX_TOKEN`.

## Instructions

1. Setup the environment variable `MAPBOX_TOKEN` to a valid [MapBox](https://www.mapbox.com) 
token.
2. Run `io.dazraf.leafymaps.tiler.AppKt.main`.

