package io.dazraf.leafymaps

import kotlin.browser.window

val L = js("L")

fun main(args: Array<String>) {
    val myMap = L.map("mapid").setView(arrayOf(-12, 0), 1.8)
    val options = js("{}")
    options.attribution = "<a href='https://cordite.foundation'>Cordite</a>"
    options.maxZoom = 4
    options.minZoom = 0
    L.tileLayer("${window.location.pathname}tiles/{z}/{x}/{y}.png", options).addTo(myMap)
}

